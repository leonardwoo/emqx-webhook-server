/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.ewhs.services;

import me.l6d.ewhs.hook.services.HookService;
import me.l6d.ewhs.mappers.WebhookMapper;
import me.l6d.ewhs.models.WebhookEntity;
import me.l6d.ewhs.models.emqx.WebHook;
import me.l6d.ewhs.utils.EntityUtil;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author Leonard Woo
 */
@Service
public class WebhookService {

  private final Logging logger = LoggingFactory.getLogging(this.getClass());

  @Autowired
  private WebhookMapper mapper;

  @Autowired
  private HookService hook;

  public boolean submit(WebHook webHook) {
    WebhookEntity entity = EntityUtil.convertWH(webHook);
    boolean flag = false;
    if (Objects.nonNull(entity)) {
      flag = mapper.add(entity) > 0;
    }
    if (flag) {
      logger.info(String.format("Data (ID: %s) insert successful.", entity.id()));
    }
    if (!hook.call(webHook.payload())) {
      logger.info("Hook execute failed.");
      flag = false;
    }
    return flag;
  }
}
