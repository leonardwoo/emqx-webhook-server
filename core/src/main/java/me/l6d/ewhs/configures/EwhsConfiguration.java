/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.ewhs.configures;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Objects;
import me.l6d.ewhs.utils.HttpUtil;
import me.l6d.ewhs.utils.JsonUtil;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.Environment;
import org.seppiko.commons.utils.ObjectUtil;
import org.seppiko.commons.utils.StreamUtil;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * @author Leonard Woo
 */
public class EwhsConfiguration {

  private final Logging log = LoggingFactory.getLogging(this.getClass());

  private static final EwhsConfiguration INSTANCE = new EwhsConfiguration();
  public static EwhsConfiguration getInstance() {
    return INSTANCE;
  }

  private EwhsConfiguration() {
    init();
  }

  private JdbcEntity jdbcConfig;
  public JdbcEntity getJdbcConfig() {
    return jdbcConfig;
  }

  private void init() {
    try {
      String filepath = System.getProperty(
              Environment.CONFIG_FILE_PARAMETER_SUFFIX, Environment.CONFIG_FILENAME_JSON);
      InputStream is = StreamUtil.getStream(
          StreamUtil.findFile(EwhsConfiguration.class, filepath) );
      if(Objects.isNull(is)) {
        throw new FileNotFoundException("config.json not found");
      }
      BufferedReader reader = StreamUtil.loadReader( is );

      JsonNode root = JsonUtil.fromJsonObject(reader);
      log.info("Config: " + root.toString());
      loadConfig(root);

      log.info("User-Agent: " + HttpUtil.UA);
    } catch (Throwable t) {
      log.error("", t);
    }
  }

  private void loadConfig(JsonNode root) {
    this.jdbcConfig = JsonUtil.toT(root.get("jdbc"), JdbcEntity.class);
  }

}
