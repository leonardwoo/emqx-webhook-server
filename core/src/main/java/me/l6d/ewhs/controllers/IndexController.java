/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.ewhs.controllers;

import me.l6d.ewhs.utils.JsonUtil;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.Map;

/**
 * @author Leonard Woo
 */
@RestController
public class IndexController {
  private final Logging log = LoggingFactory.getLogging(this.getClass());

  @RequestMapping(value = "/")
  public ModelAndView indexContentHandleExecution(
          @RequestParam(required = false) Map<String, String> params,
          @RequestBody(required = false) String requestBody) {

    log.info(JsonUtil.toJson(params));
    log.info(requestBody);

    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setView(new MappingJackson2JsonView());
    modelAndView.setStatus(HttpStatus.OK);
    modelAndView.addObject("code", 400);
    modelAndView.addObject("message", "Bad request");
    return modelAndView;
  }

}
