/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.ewhs.controllers;

import me.l6d.ewhs.models.ResponseMessageEntity;
import me.l6d.ewhs.models.emqx.WebHook;
import me.l6d.ewhs.services.WebhookService;
import me.l6d.ewhs.utils.JsonUtil;
import me.l6d.ewhs.utils.ResponseUtil;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Leonard Woo
 */
@RestController
public class WebhookController {

  private final Logging logger = LoggingFactory.getLogging(this.getClass());

  @Autowired
  private WebhookService service;

  @RequestMapping(value = "/webhook", method = RequestMethod.POST)
  public ResponseEntity<byte[]> webhookContentHandleExecution(
      @RequestBody(required = false) String requestBody) {
    if (!StringUtil.hasText(requestBody)) {
      return ResponseUtil.sendJson(200, new ResponseMessageEntity<>(400, "Can not got body"));
    }
    logger.debug(requestBody);
    WebHook webHook = JsonUtil.fromJson(requestBody, WebHook.class);
    if (webHook == null) {
      return ResponseUtil.sendJson(200, new ResponseMessageEntity<>(400, "Json parser failed"));
    }

    if (service.submit(webHook)) {
      return ResponseUtil.sendJson(200, new ResponseMessageEntity<>(200, "success"));
    }
    return ResponseUtil.sendJson(200, new ResponseMessageEntity<>(400, "failure"));
  }
}
