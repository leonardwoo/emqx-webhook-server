/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.ewhs.models.emqx;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * EMQX webhook
 *
 * @author Leonard Woo
 */
public record WebHook(@JsonProperty("username") String username,
                      @JsonProperty("topic") String topic,
                      @JsonProperty("timestamp") Long timestamp,
                      @JsonProperty("qos") Integer qos,
                      @JsonProperty("publish_received_at") Long publishReceivedAt,
                      @JsonProperty("pub_props") Map<String, Object> pubProps,
                      @JsonProperty("peerhost") String peerhost,
                      @JsonProperty("payload") String payload,
                      @JsonProperty("node") String node,
                      @JsonProperty("metadata") Metadata metadata,
                      @JsonProperty("id") String id,
                      @JsonProperty("flags") Flags flags,
                      @JsonProperty("event_type") String eventType,
                      @JsonProperty("event") String event,
                      @JsonProperty("clientid") String clientId) {

}
