/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.ewhs.utils;

import org.seppiko.commons.utils.StringUtil;

import java.util.Objects;
import java.util.regex.Matcher;

/**
 * String util extend
 *
 * @author Leonard Woo
 */
public class StringUtils {

  public static String find(String regex, String input) {
    Matcher m = StringUtil.getMatcher(regex, input);
    try {
      Objects.requireNonNull(m);
      if (m.find()) {
        return m.group(0);
      }
    } catch (NullPointerException | IllegalStateException | IndexOutOfBoundsException ignored) {
    }
    return "";
  }

  public static String getUrlHost(String url) {
    url = url.split("//")[1];
    return url.substring(0, url.indexOf("/"));
  }
}
