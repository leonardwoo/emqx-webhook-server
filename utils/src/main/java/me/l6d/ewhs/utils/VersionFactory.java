/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.ewhs.utils;

import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.StringUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Leonard Woo
 */
public class VersionFactory {

  private static final Logging logger = LoggingFactory.getLogging(VersionFactory.class);

  private VersionFactory() {
  }

  private static final String GROUP_ID = "me.l6d.ewhs";
  private static final String ARTIFACT_ID = "utils";

  public static String getVersion() {
    try (InputStream is = getPom()) {
      Properties prop = new Properties();
      prop.load(is);
      String version = prop.getProperty("version");
      if (StringUtil.isNullOrEmpty(version)) {
        version = "";
      }
      return version;
    } catch (IOException ex) {
      logger.error("loading failed " + ex.getMessage());
    }
    return "";
  }

  private static InputStream getPom() throws IOException {
    String pathname = "/META-INF/maven/" + GROUP_ID + "/" + ARTIFACT_ID + "/pom.properties";
    InputStream is = VersionFactory.class.getResourceAsStream(pathname);
//    if(is == null) {
//      is = new FileInputStream("pom.xml");
//    }
    return is;
  }

}
