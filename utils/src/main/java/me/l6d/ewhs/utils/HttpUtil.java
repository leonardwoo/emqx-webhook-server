/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.ewhs.utils;

import java.io.File;
import java.io.IOException;
import java.net.Proxy;
import java.nio.file.FileAlreadyExistsException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.StreamUtil;
import org.seppiko.commons.utils.StringUtil;
import org.seppiko.commons.utils.http.MediaType;

/**
 * Http client util
 *
 * @author Leonard Woo
 */
public class HttpUtil {

  private static final Logging logger = LoggingFactory.getLogging(HttpUtil.class);
  public static final String UA = "EmqxWebhook/" + VersionFactory.getVersion();
  private static final String APPLICATION_JSON_VALUE = MediaType.APPLICATION_JSON.getValue();
  private static final String ALL_VALUE = MediaType.ALL.getValue();
  private static final Headers.Builder HEADERS = new Headers.Builder();

  static {
    HEADERS.add("Cache-Control", "no-cache");
    HEADERS.add("Pragma", "no-cache");
    HEADERS.add("User-Agent", UA);
  }

  /**
   * Post JSON
   *
   * @param url URL
   * @param timeout timeout seconds
   * @param proxy request proxy
   * @param requestBody request json body
   * @return Response body, if exception return null
   */
  public static String postJSON(String url, int timeout, Proxy proxy, String requestBody) {
    if(!StringUtil.hasText(requestBody)) {
      return null;
    }

    try {
      OkHttpClient client = new OkHttpClient.Builder()
              .callTimeout(timeout, TimeUnit.SECONDS)
              .proxy(proxy)
              .build();

      HEADERS.add("Accept", APPLICATION_JSON_VALUE);
      HEADERS.add("Content-Type", APPLICATION_JSON_VALUE);
      Request request = new Request.Builder()
          .url(url)
          .headers(HEADERS.build())
          .post(RequestBody.create(requestBody, okhttp3.MediaType.get(APPLICATION_JSON_VALUE)))
          .build();
      try (Response response = client.newCall(request).execute()) {
        String responseBody = Objects.requireNonNull(response.body()).string();
        logger.info(response.code() + " " + responseBody);
        return responseBody;
      }
    } catch (IOException | RuntimeException ex) {
      url = StringUtils.getUrlHost(url);
      logger.error("" + url + " unexpectedly closed the connection.");
    }
    return null;
  }

  /**
   * Download with save file
   *
   * @param url URL
   * @param timeout timeout seconds
   * @param proxy download proxy
   * @param file download file
   * @return true if successful save or exists
   */
  public static boolean download(String url, int timeout, Proxy proxy, File file) {
    try {
      if (file.exists()) {
        logger.warn("Download failed",
                new FileAlreadyExistsException("File " + file.getAbsolutePath() + " already exists"));
        return true;
      }

      OkHttpClient client = new OkHttpClient.Builder()
              .connectTimeout(timeout, TimeUnit.SECONDS)
              .readTimeout(timeout, TimeUnit.SECONDS)
              .proxy(proxy)
              .build();

      HEADERS.add("Accept", ALL_VALUE);
      Request request = new Request.Builder()
          .url(url)
          .headers(HEADERS.build())
          .get()
          .build();
      try (Response response = client.newCall(request).execute()) {
        StreamUtil.writeFile(Objects.requireNonNull(response.body()).bytes(), file);
        return true;
      } catch (NullPointerException ex) {
        logger.error("Not found response body");
      }
    } catch (IOException | RuntimeException ex) {
      url = StringUtils.getUrlHost(url);
      logger.error("" + url + " unexpectedly closed the connection.");
    }
    return false;
  }
}
