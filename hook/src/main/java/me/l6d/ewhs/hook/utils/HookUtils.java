/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.ewhs.hook.utils;

import me.l6d.ewhs.utils.HttpUtil;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.http.URIUtil;

import java.io.File;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Leonard Woo
 */
public class HookUtils {

  private static final Logging log = LoggingFactory.getLogging(HookUtils.class);

  private HookUtils() {}

  /**
   *
   * @param urls multi-url(list)
   * @param timeout download timeout (seconds)
   * @param proxy download proxy
   * @param pathname save pathname
   * @return true if download and save file successful
   */
  public static boolean download(final ArrayList<String> urls, final int timeout, final Proxy proxy, final String pathname,
                                 final int retryIntervalSeconds, final int numberOfRetries) {
    final String finalPathname;
    if (!pathname.endsWith(File.separator)) {
      finalPathname = pathname + File.separator;
    } else {
      finalPathname = pathname;
    }

    File path = new File(finalPathname);
    try {
      if (!path.exists()) {
        if (!path.mkdirs()) {
          log.error("Directory <" + finalPathname + "> does not exist and cannot be created.");
          return false;
        }
      }
    } catch (SecurityException ex) {
      log.error("", ex);
    }

    AtomicBoolean lastFlag = new AtomicBoolean(false);
    urls.forEach(url -> {
      try {
        CompletableFuture<Boolean> futureAsync = CompletableFuture.supplyAsync(() -> {
          File file = new File(finalPathname + URIUtil.getLastPathname(url));
          boolean dlflag;
          int retry = 0;
          do {
            dlflag = HttpUtil.download(url, timeout, proxy, file);
            if (dlflag) {
              break;
            }
            try {
              TimeUnit.SECONDS.sleep(retryIntervalSeconds);
            } catch (InterruptedException ignored) {}
            retry++;
          } while (retry < numberOfRetries);
          return dlflag;
        });
        lastFlag.set(futureAsync.get());
      } catch (RuntimeException | ExecutionException | InterruptedException ex) {
        log.warn("Download failed " + ex.getMessage());
      }
    });
    return lastFlag.get();
  }
}
