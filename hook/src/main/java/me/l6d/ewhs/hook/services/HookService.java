/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.l6d.ewhs.hook.services;

import me.l6d.ewhs.hook.models.*;
import me.l6d.ewhs.hook.utils.*;
import me.l6d.ewhs.utils.*;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;
import org.seppiko.commons.utils.StringUtil;
import org.springframework.stereotype.Service;


/**
 * EWHS hook service
 *
 * @author Leonard Woo
 */
@Service
public class HookService {

  private final Logging log = LoggingFactory.getLogging(this.getClass());

  /**
   * When webhook service finish call this method
   *
   * @param payload MQTT message
   * @return true if successful, false otherwise
   */
  public boolean call(String payload) {
    if (StringUtil.isNullOrEmpty(payload)) {
      return false;
    }

    // TODO: do something

    return true;
  }

}